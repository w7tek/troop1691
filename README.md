# Troop 1691 Website #

This is the source code repository for [a Boy Scout Troop website](http://troop1691.bcscouts.org/).

### What is [this repository](https://bitbucket.org/w7tek/troop1691) for? ###

* Boy scouts can get first-hand experience building a web site and using professional software development tools and processes.
* Parents and other adult volunteers can learn and help the boys, too!
* Issue (bug) tracking - it's a tool that professional programmers use for proposing changes to a project and prioritizing work (choosing what to work on first); it's also for tracking and reporting progress on the project over time.

### How do I get started? ###

Initially, this repository contains only the front-end browser client app (in the 'app' folder). Eventually, source code for a backend server will also be found here.

1. Download and install the [SourceTree git client](https://www.sourcetreeapp.com/) for your computer.
1. After installing SourceTree, click on the "down-pointing-arrow" button on the top row of [this project's BitBucket project page](https://bitbucket.org/w7tek/troop1691), then choose the "Clone in SourceTree" link. SourceTree will prompt you where you want to clone the repository; remember where you decided to put it.
1. After cloning the repository in SourceTree, you will have a copy on your computer of all of the files for this project, along with the history of changes those files have gone through. For now, that's not so important, but over time, you will learn how to use SourceTree to collaborate with other people who are also working on this project at the same time.
1. Remember where you told SourceTree to put the cloned repository? Open those files using your favorite web programmer's editor. [Brackets](http://brackets.io/) is one freely-available choice.
1. Experiment making changes, and open the changed file in your web browser to see the effect of your changes. Ask questions. Don't be afraid to try stuff, that's one of the best ways to learn.
1. Your imagination (and the troop's budget) are the only limits on what you can do with this thing...
1. Any time you are stuck or have questions, email [Tommy Knowlton](mailto:1691.upside.down@gmail.com) and I'll try to help. Be warned, it might be awhile before I can reply to email but I will try to be quick about it.