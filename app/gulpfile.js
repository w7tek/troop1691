"use strict";
(function (gulp, del, imagemin, rename, lessCss,
           cdnizer, useref, LessPluginCleanCSS, LessPluginAutoPrefix, browserSync) {

    var cleancss = new LessPluginCleanCSS({advanced: true}),
        autoprefix = new LessPluginAutoPrefix({browsers: ["last 2 versions"]});

    gulp.task('clean', function (cb) {
        del('dist/', cb);
    });

    var sourcePaths = {};

    sourcePaths['less'] = ['less/site.less'];

    gulp.task('less', function () {
        return gulp.src(sourcePaths['less'])
            .pipe(lessCss({
                plugins: [autoprefix, cleancss]
            }))
            .pipe(rename({suffix: 'min'}))
            .pipe(gulp.dest('dist/css/'))
            .pipe(browserSync.reload({stream: true}));
    });

    sourcePaths['img'] = ['images/**/*.png'];

    gulp.task('img', function () {
        return gulp.src(sourcePaths['img'])
            .pipe(imagemin({optimizationLevel: 5}))
            .pipe(gulp.dest('dist/images/'));
    });

    sourcePaths['html'] = ['home.html'];

    gulp.task('html', function () {
        return gulp.src(sourcePaths['html'])
            //.pipe(cdnizer({
            //    files: [
            //    ]
            //}))
            .pipe(useref())
            .pipe(gulp.dest('dist'));
    });

    gulp.task('img-watch', ['img'], browserSync.reload);
    gulp.task('html-watch', ['html'], browserSync.reload);

    gulp.task('serve', function() {
        browserSync({
            server: {
                baseDir: 'dist'
            }
        });

        gulp.watch(sourcePaths['less'], ['less']);
        gulp.watch(sourcePaths['img'], ['img', 'img-watch']);
        gulp.watch(sourcePaths['html'], ['html', 'html-watch']);
    });

    gulp.task('default', ['serve', 'less', 'html', 'img']);

})(require('gulp'),
    require('del'),
    require('gulp-imagemin'),
    require('gulp-rename'),
    require('gulp-less'),
    require('gulp-cdnizer'),
    require('gulp-useref'),
    require('less-plugin-clean-css'),
    require('less-plugin-autoprefix'),
    require('browser-sync'));
